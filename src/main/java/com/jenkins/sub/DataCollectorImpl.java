package com.jenkins.sub;


import com.jenkins.model.Jenkins;
import com.offbytwo.jenkins.model.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataCollectorImpl implements DataCollector{
    private final Logger logger = LoggerFactory.getLogger(DataCollectorImpl.class);

    @Autowired
    private JenkinsApiInfoCollector jenkinsInfoCollector;

    @Autowired
    private JenkinsXMLConfigInfoCollector jenkinsConfigInfoCollector;


    @Override
    public Jenkins collectJobDetails(Job job, String url) {
        Jenkins jenkins=new Jenkins();
        logger.info("Trying...");
        jenkinsInfoCollector.extract(job,jenkins);
        jenkinsConfigInfoCollector.extract(url,job,jenkins);
        return jenkins;
    }

    @Override
    public Jenkins recover(RuntimeException e,Job job, String url) {
        logger.error("Could not get details of Job {} after max retries");
        return null;
    }
}
