package com.jenkins.sub;

import com.jenkins.model.Jenkins;
import com.offbytwo.jenkins.model.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Optional;

@Component
public class JenkinsXMLConfigInfoCollector {

    private final Logger logger = LoggerFactory.getLogger(JenkinsXMLConfigInfoCollector.class);

    public void extract(String url,Job job, Jenkins jenkins){
        String configUrl = buildConfigUrl(url, job);
        String configXML = retrieveConfigXMLFromServer(configUrl);
        String triggers = getTriggers(configXML);
        jenkins.setTriggers(Optional.ofNullable(triggers).orElse("-"));
    }

    private String buildConfigUrl(String url,Job job){
        String finalUrl=url+"/job/"+job.getName()+"/config.xml";
        return finalUrl;
    }

    private String retrieveConfigXMLFromServer(String configUrl){
        RestTemplate restTemplate=new RestTemplate();
        ResponseEntity<String> forEntity = restTemplate.getForEntity(configUrl, String.class);
        if(forEntity.getStatusCode()== HttpStatus.OK){
            return forEntity.getBody();
        }else {
            logger.error("Unexpected status {} returned from the server",forEntity.getStatusCode());
            throw new RuntimeException("Unexpected response code");
        }
    }

    private String getTriggers(String xml){
        String triggerStr=null;
        try {
            Document doc=generateDomObject(xml);
            Optional<NodeList> trigger = retrieveNodeList(doc, "triggers");
            if(trigger.isPresent()&&trigger.get().item(0)!=null){
                NodeList childNodes = trigger.get().item(0).getChildNodes();
                for(int i=0;i<childNodes.getLength();i++){
                    if(!childNodes.item(i).getNodeName().toString().trim().equalsIgnoreCase("#text")){
                        if(triggerStr!=null)
                            triggerStr=triggerStr+" | " +childNodes.item(i).getNodeName().toString();
                        else
                            triggerStr=childNodes.item(i).getNodeName().toString();
                    }
                }
            }

        } catch (ParserConfigurationException|SAXException|IOException e) {
           logger.error("Error while parsing",e);
           throw new RuntimeException(e);
        }

        return triggerStr;
    }

    private Optional<NodeList> retrieveNodeList(Document doc, String tagName){
        Element rootElement = doc.getDocumentElement();
        NodeList trigger = doc.getElementsByTagName(tagName);
        return Optional.ofNullable(trigger);
    }

    private Document generateDomObject(String xml) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes("utf-8"))));
        doc.getDocumentElement().normalize();
        return doc;
    }
}
