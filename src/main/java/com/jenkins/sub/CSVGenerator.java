package com.jenkins.sub;

import com.jenkins.model.Jenkins;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

@Component
public class CSVGenerator {

    private Jenkins buildHeaders() {
        Jenkins jenkins=new Jenkins();
        jenkins.setTriggers("Triggers");
        jenkins.setLastSuccessfulBuildDate("LastSuccessfulBuildDate");
        jenkins.setLastBuildDate("LastBuildDate");
        jenkins.setUpstreamProject("UpstreamProject");
        jenkins.setDownStreamProject("DownStreamProject");
        jenkins.setName("Name");
        jenkins.setLastBuildYear("LastRunYear");
        jenkins.setLastSuccessfulBuildYear("LastSuccessfulRunYear");
        jenkins.setUrl("URL");
        //jenkins.setDescription("Description");
        return jenkins;
    }

    private  String jenkinsAsCSVStr(Jenkins jenkins){
        String s=jenkins.getName()+
                ","+jenkins.getTriggers()+
                ","+jenkins.getDownStreamProject()+
                ","+jenkins.getUpstreamProject()+
                ","+jenkins.getLastBuildDate()+
                ","+jenkins.getLastSuccessfulBuildDate()+
                ","+jenkins.getLastBuildYear()+
                ","+jenkins.getLastSuccessfulBuildYear()+
                ","+jenkins.getUrl()+
                //","+jenkins.getDescription().replace("\n","-")+
                "\n";
        return s;
    }

    public void writeAllToCSV(List<Jenkins> list, String fileStr) throws FileNotFoundException {
        try(PrintWriter writer=new PrintWriter(new File(fileStr))) {
            writer.append(jenkinsAsCSVStr(buildHeaders()));
            list.stream().forEach(jenkins -> {
                writer.append(jenkinsAsCSVStr(jenkins));
            });
        }
    }
}
