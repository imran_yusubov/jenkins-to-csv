package com.jenkins.sub;

import com.jenkins.model.Jenkins;
import com.offbytwo.jenkins.model.Job;
import com.offbytwo.jenkins.model.JobWithDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

@Component
public class JenkinsApiInfoCollector {

    private final Logger logger = LoggerFactory.getLogger(JenkinsApiInfoCollector.class);

    public void extract(Job job,Jenkins jenkins) {
        try {
            jenkins.setName(Optional.ofNullable(job.getName()).orElse("-"));
            jenkins.setUrl(Optional.ofNullable(job.getUrl()).orElse("-"));
            JobWithDetails details = job.details();
            getDownStream(details,jenkins);
            getUpStream(details,jenkins);
            getLastBuildDate(details,jenkins);
            //jenkins.setDescription(Optional.ofNullable(job.details().getDescription()).orElse("-"));
            //logger.info(job.details().getDescription());
            getLastSuccessfulBuildDate(details,jenkins);
        }catch (IOException e) {
            logger.error("IOException while reading ",e);
            throw new RuntimeException(e);
        }

    }

    private void getLastBuildDate(JobWithDetails details,Jenkins jenkins) {
        try{
            if(details==null
                    ||details.getLastBuild()==null
                    ||details.getLastBuild().details()==null){
                jenkins.setLastBuildDate("-");
                return;
            }

            long timestamp = details.getLastBuild().details().getTimestamp();
            Date date=new Date(timestamp);
            jenkins.setLastBuildDate(date.toString());
            jenkins.setLastBuildYear(getYear(date));
        }catch (IOException e){
            logger.error("Exception while reading", e);
            throw new RuntimeException(e);
        }
    }

    private void getLastSuccessfulBuildDate(JobWithDetails details,Jenkins jenkins) {
        try{
            if(details==null
                    ||details.getLastSuccessfulBuild()==null
                    ||details.getLastSuccessfulBuild().details()==null){
                jenkins.setLastSuccessfulBuildDate("-");
                return;
            }

            long timestamp = details.getLastSuccessfulBuild().details().getTimestamp();
            Date date=new Date(timestamp);
            jenkins.setLastSuccessfulBuildDate(date.toString());
            jenkins.setLastSuccessfulBuildYear(getYear(date));
        }catch (IOException e){
            logger.error("Exception while reading", e);
            throw new RuntimeException(e);
        }
    }


    private String getYear(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR)+"";
    }

    private  void getDownStream(JobWithDetails details, Jenkins jenkins) {
        String s=getProjectsNamesAsString(details.getDownstreamProjects());
        jenkins.setDownStreamProject(s);
    }

    private  void getUpStream(JobWithDetails details, Jenkins jenkins) {
        String s=getProjectsNamesAsString(details.getUpstreamProjects());
        jenkins.setUpstreamProject(s);
    }

    private  String getProjectsNamesAsString(List<Job> projects) {
        String s="";
        if(projects.size()==0)
            return s="-";

        for(Job job: projects){
            if(s!=null&&s.length()>0)
                s=s+" | "+job.getName();
            else
                s=job.getName();
        }
        return s;
    }


}
