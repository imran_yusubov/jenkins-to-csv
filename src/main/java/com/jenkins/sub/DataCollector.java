package com.jenkins.sub;

import com.jenkins.model.Jenkins;
import com.offbytwo.jenkins.model.Job;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;

public interface DataCollector {

    @Retryable(
            value = { RuntimeException.class },
            maxAttempts = 4,
            backoff = @Backoff(delay = 1000))
    Jenkins collectJobDetails(Job job, String url);

    @Recover
    Jenkins recover(RuntimeException e,Job job, String url);
}
