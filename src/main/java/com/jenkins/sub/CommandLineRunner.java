package com.jenkins.sub;

import com.jenkins.model.Jenkins;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Component
public class CommandLineRunner implements org.springframework.boot.CommandLineRunner {

    private final Logger logger = LoggerFactory.getLogger(CommandLineRunner.class);

    @Autowired
    private CSVGenerator csvGenerator;

    @Autowired
    private DataCollector dataCollector;

    @Override
    public void run(String... strings) throws Exception {
        validateInput(strings);
        logCommandLineArguments(strings);
        List<Jenkins> jenkins = startProcessing(strings[0], strings[1], strings[2]);
        csvGenerator.writeAllToCSV(jenkins,strings[3]);

    }

    private void logCommandLineArguments(String[] strings) {
        logger.info("Started processing data...");
        logger.info("Job URL: {} ",strings[0]);
        logger.info("Login: {}, password {}",strings[1],"*********");
        logger.info("Output: {} ",strings[3]);
    }


    private List<Jenkins> startProcessing(String url, String user, String pwd) throws IOException, URISyntaxException {
        JenkinsServer server = new JenkinsServer(new URI(url), user, pwd);
        Map<String, Job> jobs = server.getJobs();
        List<Jenkins> jenkinsList=new ArrayList<>();
        for (String jobName:server.getJobs().keySet()){
            logger.info("Processing : {}",jobName);
            Job job = jobs.get(jobName);
            Jenkins jenkins=getJobDetails(job,url);
            jenkinsList.add(jenkins);
        }
        return jenkinsList;
    }


    public Jenkins getJobDetails(Job job, String url) {
        return dataCollector.collectJobDetails(job,url);
    }


    private void validateInput(String[] strings) {
        if(strings.length<4){
            printUsage();
            System.exit(1);
        }
    }

    private void printUsage() {
        logger.error("Usage, pass the arguments : JenkinsURL, login, password, full path to the output file");
    }

}