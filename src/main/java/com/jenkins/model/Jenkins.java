package com.jenkins.model;

public class Jenkins {

    private String name;
    private String upstreamProject;
    private String downStreamProject;
    private String lastSuccessfulBuildDate;
    private String lastBuildDate;
    private String triggers;
    private String lastBuildYear;
    private String lastSuccessfulBuildYear;
    private String url;
    private String status;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpstreamProject() {
        return upstreamProject;
    }

    public void setUpstreamProject(String upstreamProject) {
        this.upstreamProject = upstreamProject;
    }

    public String getDownStreamProject() {
        return downStreamProject;
    }

    public void setDownStreamProject(String downStreamProject) {
        this.downStreamProject = downStreamProject;
    }

    public String getLastSuccessfulBuildDate() {
        return lastSuccessfulBuildDate;
    }

    public void setLastSuccessfulBuildDate(String lastSuccessfulBuildDate) {
        this.lastSuccessfulBuildDate = lastSuccessfulBuildDate;
    }

    public String getLastBuildDate() {
        return lastBuildDate;
    }

    public void setLastBuildDate(String lastBuildDate) {
        this.lastBuildDate = lastBuildDate;
    }

    public String getTriggers() {
        return triggers;
    }

    public void setTriggers(String triggers) {
        this.triggers = triggers;
    }

    public String getLastBuildYear() {
        return lastBuildYear;
    }

    public void setLastBuildYear(String lastBuildYear) {
        this.lastBuildYear = lastBuildYear;
    }

    public String getLastSuccessfulBuildYear() {
        return lastSuccessfulBuildYear;
    }

    public void setLastSuccessfulBuildYear(String lastSuccessfulBuildYear) {
        this.lastSuccessfulBuildYear = lastSuccessfulBuildYear;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
